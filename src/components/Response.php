<?php

namespace semako\queue\components;

use yii;
use yii\helpers\Json;
use yii\web\Response as BaseResponse;

/**
 * Class Response
 * @package semako\queue\components
 */
class Response
{
    /**
     * @var BaseResponse
     */
    private $response;
    
    /**
     * Response constructor.
     */
    public function __construct()
    {
        $this->response = Yii::$app->getResponse();
        $this->setReady(false);
        $this->setProgress(0);
        $this->setCount(0);
        $this->setLink([]);
    }

    /**
     * @param bool $ready
     * @return $this
     */
    public function setReady($ready)
    {
        $this->response->getHeaders()->set('x-ready', (int) $ready);
        return $this;
    }

    /**
     * @param int $progress
     * @return $this
     */
    public function setProgress($progress)
    {
        $progress = (int) $progress;
        $this->response->getHeaders()->set('x-progress', $progress > 100 ? 100 : ($progress < 0 ? 0 : $progress));
        return $this;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setCount($count)
    {
        $this->response->getHeaders()->set('x-total-count', (int) $count);
        return $this;
    }

    /**
     * @param array $link
     * @return $this
     */
    public function setLink(array $link)
    {
        $this->response->getHeaders()->set('link', implode(',', $link));
        return $this;
    }

    /**
     * @param mixed $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->response->content = Json::encode($content);
        return $this;
    }

    /**
     * @return BaseResponse
     */
    public function getResponse()
    {
        return $this->response;
    }
}

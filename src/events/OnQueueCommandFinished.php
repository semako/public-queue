<?php

namespace semako\queue\events;

use semako\eventsManager\components\Event;
use semako\queue\interfaces\events\IOnQueueCommandFinished;

/**
 * Class OnQueueCommandFinished
 * @package semako\queue\events
 */
class OnQueueCommandFinished extends Event implements IOnQueueCommandFinished
{
}

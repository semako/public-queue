<?php

namespace semako\queue\interfaces;

use semako\eventsManager\interfaces\IEventsManager as IBaseEventsManager;
use semako\queue\interfaces\events\IOnQueueCommandFailed;
use semako\queue\interfaces\events\IOnQueueCommandFinished;
use semako\queue\interfaces\events\IOnQueueCommandStarted;
use semako\queue\interfaces\events\IOnQueueCommandUpdated;

/**
 * Interface IEventsManager
 * @package semako\queue\interfaces
 */
interface IEventsManager extends IBaseEventsManager
{
    const ON_QUEUE_COMMAND_STARTED  = 'onQueueCommandStarted';
    const ON_QUEUE_COMMAND_FINISHED = 'onQueueCommandFinished';
    const ON_QUEUE_COMMAND_FAILED   = 'onQueueCommandFailed';
    const ON_QUEUE_COMMAND_UPDATED  = 'onQueueCommandUpdated';

    /**
     * @param IOnQueueCommandStarted $event
     * @return void
     */
    public function onQueueCommandStarted(IOnQueueCommandStarted $event);

    /**
     * @param IOnQueueCommandFinished $event
     * @return void
     */
    public function onQueueCommandFinished(IOnQueueCommandFinished $event);

    /**
     * @param IOnQueueCommandFailed $event
     * @return void
     */
    public function onQueueCommandFailed(IOnQueueCommandFailed $event);

    /**
     * @param IOnQueueCommandUpdated $event
     * @return void
     */
    public function onQueueCommandUpdated(IOnQueueCommandUpdated $event);
}

<?php

namespace semako\queue\models;

use semako\queue\enums\TableName;
use semako\userVk\models\User;
use yii;
use semako\queue\models\query\CommandQuery;
use semako\queue\interfaces\models\ICommand;
use semako\yii2Common\components\ActiveRecord;
use yii\db\Connection;

/**
 * This is the model class for table "{{%command}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $pid
 * @property integer $progress
 * @property string $ns
 * @property boolean $is_running
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $finished_at
 * @property string $data
 * @property string $response
 * @property integer $priority
 * @property boolean $status
 */
class Command extends ActiveRecord implements ICommand
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%' . TableName::COMMAND . '}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'ns', 'created_at', 'priority'], 'required'],
            [['id_user', 'pid', 'progress', 'created_at', 'updated_at', 'finished_at', 'priority', 'status'], 'integer'],
            [['is_running'], 'boolean'],
            [['data', 'response'], 'string'],
            [['ns'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'pid' => Yii::t('app', 'PID'),
            'progress' => Yii::t('app', 'Progress'),
            'ns' => Yii::t('app', 'Ns'),
            'is_running' => Yii::t('app', 'Is Running'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'finished_at' => Yii::t('app', 'Finished At'),
            'data' => Yii::t('app', 'Data'),
            'response' => Yii::t('app', 'Response'),
            'priority' => Yii::t('app', 'Priority'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return CommandQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommandQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getNs()
    {
        return $this->ns;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return int
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @return User|array|null
     */
    public function getUser()
    {
        return User::find()->byPk($this->id_user)->one();
    }

    /**
     * Returns the database connection used by this AR class.
     * By default, the "db" application component is used as the database connection.
     * You may override this method if you want to use a different database connection.
     * @return Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->getDbAsync();
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return (bool) $this->status;
    }

    /**
     * @return bool
     */
    public function getIsRunning()
    {
        return (bool) $this->is_running;
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * @inheritdoc
     *
     * The default implementation returns the names of the columns whose values have been populated into this record.
     */
    public function fields()
    {
        return [
            'id'         => 'id',
            'progress'   => 'progress',
            'is_running' => function () {
                return $this->getIsRunning();
            },
            'status' => function () {
                return $this->getStatus();
            },
        ];
    }
}

<?php

namespace semako\queue\components;

use semako\queue\enums\CommandStatus;
use yii;
use yii\base\Component;
use yii\helpers\Json;
use semako\queue\interfaces\IQueue;
use semako\queue\interfaces\IQueueRequest;
use Pheanstalk\Pheanstalk;
use Pheanstalk\PheanstalkInterface;
use semako\queue\models\Command;
use semako\queue\interfaces\models\ICommand;
use semako\queue\manage\CommandProcessManage;

/**
 * Class Queue
 * @package semako\queue\components
 */
class Queue extends Component implements IQueue
{
    const DELAY = 5;

    /**
     * @var string
     */
    public $tube;

    /**
     * @var string
     */
    public $host;

    /**
     * @var int
     */
    public $port;

    /**
     * @var PheanstalkInterface
     */
    private $connection;

    /**
     * @return string
     */
    public function getTube()
    {
        return $this->tube;
    }

    /**
     * @param string $tube
     */
    public function setTube($tube)
    {
        $this->tube = $tube;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return PheanstalkInterface
     */
    private function getConnection()
    {
        return $this->connection;
    }

    /**
     *
     */
    public function init()
    {
        $this->connection = new Pheanstalk($this->getHost(), $this->getPort());
        parent::init();
    }

    /**
     *
     */
    public function serve()
    {
        $job = $this->getConnection()
            ->watch($this->getTube())
            ->reserve();

        $data = $job->getData();
        $this->getConnection()->delete($job);
        
        $decoded = Json::decode(Yii::$app->getSecurity()->decryptByKey($data, Yii::$app->params['key']));
        
        if (!$decoded || !isset($decoded['id'])) {
            return;
        }

        /** @var ICommand $command */
        $command = Command::find()->byPk($decoded['id'])->one();
        
        if (!$command) {
            return;
        }

        (new CommandProcessManage($command))->run();
    }

    /**
     * @param IQueueRequest $request
     * @param int $delay
     * @return ICommand
     */
    public function push(IQueueRequest $request, $delay = 0)
    {
        $priority = 4294967295;

        $command             = new Command();
        $command->id_user    = $request->getUserId();
        $command->progress   = 0;
        $command->ns         = $request->getNs();
        $command->is_running = false;
        $command->data       = Json::encode($request->getData());
        $command->priority   = $request->getPriority();
        $command->status     = CommandStatus::OK;
        $command->save();

        $encoded = Yii::$app->getSecurity()->encryptByKey(Json::encode([
            'id' => $command->getPk(),
        ]), Yii::$app->params['key']);
        
        $this->getConnection()
            ->useTube($this->getTube())
            ->put($encoded, $priority, $delay);

        return $command;
    }
}

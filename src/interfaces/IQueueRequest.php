<?php

namespace semako\queue\interfaces;

/**
 * Interface IQueueRequest
 * @package semako\queue\interfaces
 */
interface IQueueRequest
{
    /**
     * @return int
     */
    public function getUserId();

    /**
     * @return mixed
     */
    public function getData();

    /**
     * @return string
     */
    public function getNs();

    /**
     * @return int
     */
    public function getPriority();
}

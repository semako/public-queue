<?php

use yii\base\Component;
use yii\base\Security;
use yii\BaseYii;
use yii\db\Connection;

/**
 * Class Yii
 */
class Yii extends BaseYii
{
    /**
     * @var Web
     */
    public static $app;
}

/**
 * Class Web
 * @method Connection getDbAsync()
 * @method Security getSecurity()
 */
class Web extends Component
{
}

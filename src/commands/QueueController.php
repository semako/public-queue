<?php

namespace semako\queue\commands;

use yii;
use yii\console\Controller;

/**
 * Class QueueController
 * @package semako\queue\commands
 */
class QueueController extends Controller
{
    /**
     * Starting Queue by name
     * @param string $name Name of queue
     */
    public function actionStart($name)
    {
        exec(Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'yii queue/serve ' . $name . ' > /dev/null 2>/dev/null &');
    }

    /**
     * Stop Queue by name
     * @param string $name Queue name
     */
    public function actionStop($name)
    {
        $file = Yii::getAlias('@runtime') . '/' . $name . '.pid';
        $pid  = file_get_contents($file);
        exec('kill -9 ' . $pid);
        unlink($file);
    }

    /**
     * Starting Queue Server by name
     * @param string $name Queue name
     */
    public function actionServe($name)
    {
        $file = Yii::getAlias('@runtime') . '/' . $name . '.pid';
        $pid  = getmypid();

        file_put_contents($file, $pid);

        Yii::$app->getQueue()->serve();

        unlink($file);
    }
}

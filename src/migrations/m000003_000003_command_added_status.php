<?php

namespace semako\queue\migrations;

use semako\queue\enums\TableName;
use yii\db\Migration;

/**
 * Class m000003_000003_command_added_status
 */
class m000003_000003_command_added_status extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        $this->addColumn(TableName::COMMAND, 'status', $this->boolean()->notNull()->defaultValue(true));
    }

    /**
     *
     */
    public function safeDown()
    {
        $this->dropColumn(TableName::COMMAND, 'status');
    }
}

<?php

namespace semako\queue\interfaces;

use semako\yii2Common\interfaces\IManage;

/**
 * Interface ICommandRun
 * @package semako\queue\interfaces
 */
interface ICommandRun extends IManage
{
}

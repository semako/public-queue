<?php

namespace semako\queue\components;

use semako\queue\interfaces\IQueueRequest;

/**
 * Class QueueRequest
 * @package semako\queue\components
 */
class QueueRequest implements IQueueRequest
{
    /**
     * @var mixed
     */
    private $data;

    /**
     * @var string
     */
    private $ns;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var int
     */
    private $priority;

    /**
     * QueueRequest constructor.
     * @param int $userId
     * @param string $ns
     * @param mixed $data
     * @param int $priority
     */
    public function __construct($userId, $ns, $data, $priority)
    {
        $this->userId   = $userId;
        $this->ns       = $ns;
        $this->data     = $data;
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getNs()
    {
        return $this->ns;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
}

<?php

namespace semako\queue\interfaces\models;

use semako\userVk\models\User;
use semako\yii2Common\interfaces\IActiveRecord;

/**
 * Interface ICommand
 * @package semako\queue\interfaces\models
 */
interface ICommand extends IActiveRecord
{
    /**
     * @return string
     */
    public function getData();

    /**
     * @return string
     */
    public function getNs();

    /**
     * @return string
     */
    public function getResponse();

    /**
     * @return int
     */
    public function getPid();

    /**
     * @return int
     */
    public function getPriority();

    /**
     * @return bool
     */
    public function getIsRunning();

    /**
     * @return User|array|null
     */
    public function getUser();

    /**
     * @return bool
     */
    public function getStatus();

    /**
     * @return int
     */
    public function getProgress();
}

<?php

namespace semako\queue\traits;

use semako\queue\interfaces\IQueue;
use yii;

/**
 * Class Queue
 * @package semako\queue\traits
 * @property IQueue $queue
 */
trait Queue
{
    /**
     * @return IQueue
     */
    public function getQueue()
    {
        return Yii::$app->queue;
    }
}

<?php

namespace semako\queue\components;

use semako\eventsManager\components\EventsManager as BaseEventsManager;
use semako\queue\interfaces\events\IOnQueueCommandFailed;
use semako\queue\interfaces\events\IOnQueueCommandFinished;
use semako\queue\interfaces\events\IOnQueueCommandStarted;
use semako\queue\interfaces\events\IOnQueueCommandUpdated;
use semako\queue\interfaces\IEventsManager;
use semako\queue\manage\CommandModelManage;
use yii;
use yii\helpers\Json;
use yii\log\Logger;

/**
 * Class Events
 * @package semako\queue\components
 */

class EventsManager extends BaseEventsManager implements IEventsManager
{
    /**
     *
     */
    protected function register()
    {
        Yii::$app->on(IEventsManager::ON_QUEUE_COMMAND_STARTED, function (IOnQueueCommandStarted $event) {
            CommandModelManage::start($event);
            Yii::getLogger()->log('Command started: ' . Json::encode($event->getMessage()), Logger::LEVEL_WARNING);
        });

        Yii::$app->on(IEventsManager::ON_QUEUE_COMMAND_FINISHED, function (IOnQueueCommandFinished $event) {
            CommandModelManage::finish($event);
            Yii::getLogger()->log('Command finished: ' . Json::encode($event->getMessage()), Logger::LEVEL_WARNING);
        });

        Yii::$app->on(IEventsManager::ON_QUEUE_COMMAND_FAILED, function (IOnQueueCommandFailed $event) {
            CommandModelManage::fail($event);
            Yii::getLogger()->log('Command failed: ' . Json::encode($event->getMessage()), Logger::LEVEL_WARNING);
        });

        Yii::$app->on(IEventsManager::ON_QUEUE_COMMAND_UPDATED, function (IOnQueueCommandUpdated $event) {
            CommandModelManage::update($event);
            Yii::getLogger()->log('Command updated: ' . Json::encode($event->getMessage()), Logger::LEVEL_WARNING);
        });
    }

    /**
     * @param IOnQueueCommandStarted $event
     * @return void
     */
    public function onQueueCommandStarted(IOnQueueCommandStarted $event)
    {
        $this->raise(__FUNCTION__, $event);
    }

    /**
     * @param IOnQueueCommandFinished $event
     * @return void
     */
    public function onQueueCommandFinished(IOnQueueCommandFinished $event)
    {
        $this->raise(__FUNCTION__, $event);
    }

    /**
     * @param IOnQueueCommandFailed $event
     * @return void
     */
    public function onQueueCommandFailed(IOnQueueCommandFailed $event)
    {
        $this->raise(__FUNCTION__, $event);
    }

    /**
     * @param IOnQueueCommandUpdated $event
     * @return void
     */
    public function onQueueCommandUpdated(IOnQueueCommandUpdated $event)
    {
        $this->raise(__FUNCTION__, $event);
    }
}

<?php

namespace semako\queue\events;

use semako\eventsManager\components\Event;
use semako\queue\interfaces\events\IOnQueueCommandStarted;

/**
 * Class OnQueueCommandStarted
 * @package semako\queue\events
 */
class OnQueueCommandStarted extends Event implements IOnQueueCommandStarted
{
}

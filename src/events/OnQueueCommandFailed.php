<?php

namespace semako\queue\events;

use semako\eventsManager\components\Event;
use semako\queue\interfaces\events\IOnQueueCommandFailed;

/**
 * Class OnQueueCommandFailed
 * @package semako\queue\events
 */
class OnQueueCommandFailed extends Event implements IOnQueueCommandFailed
{
}

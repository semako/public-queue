<?php

namespace semako\queue\exceptions;

use yii\web\NotAcceptableHttpException;

/**
 * Class AlreadyHasRunningCommand
 * @package semako\queue\exceptions
 */
class AlreadyHasRunningCommand extends NotAcceptableHttpException
{
}

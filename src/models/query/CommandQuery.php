<?php

namespace semako\queue\models\query;

use semako\yii2Common\components\ActiveQuery;
use yii\helpers\Json;

/**
 * This is the ActiveQuery class for [[\semako\queue\models\Command]].
 * @see \semako\queue\models\Command
 */
class CommandQuery extends ActiveQuery
{
    /**
     * @param $data
     * @return CommandQuery
     */
    public function byData($data)
    {
        return $this->andWhere([
            'data' => Json::encode($data),
        ]);
    }

    /**
     * @return CommandQuery
     */
    public function isRunning()
    {
        return $this->andWhere([
            'is_running' => true
        ]);
    }

    /**
     * @param int $id
     * @return CommandQuery
     */
    public function byUserId($id)
    {
        return $this->andWhere([
            'id_user' => (int) $id,
        ]);
    }

    /**
     * @return CommandQuery
     */
    public function isFinished()
    {
        return $this
            ->andWhere([
                'is_running' => false,
            ])
            ->andWhere([
                'not', ['finished_at' => null]
            ]);
    }
}

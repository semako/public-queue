<?php

namespace semako\queue\interfaces;

use semako\queue\interfaces\models\ICommand;

/**
 * Interface IQueue
 * @package semako\queue\interfaces
 */
interface IQueue
{
    /**
     * @return void
     */
    public function serve();

    /**
     * @param IQueueRequest $request
     * @param int $delay
     * @return ICommand
     */
    public function push(IQueueRequest $request, $delay = 0);
}

<?php

namespace semako\queue\interfaces\events;

use semako\eventsManager\interfaces\IEvent;
use semako\queue\interfaces\models\ICommand;

/**
 * Interface IOnQueueCommandFinished
 * @package semako\queue\interfaces\events
 */
interface IOnQueueCommandFinished extends IEvent
{
    /**
     * @return ICommand
     */
    public function &getMessage();
}

<?php

namespace semako\queue\interfaces\events;

use semako\eventsManager\interfaces\IEvent;
use semako\queue\interfaces\models\ICommand;

/**
 * Interface IOnQueueCommandStarted
 * @package semako\queue\interfaces\events
 */
interface IOnQueueCommandStarted extends IEvent
{
    /**
     * @return ICommand
     */
    public function &getMessage();
}

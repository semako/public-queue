<?php

namespace semako\queue\migrations;

use semako\queue\enums\TableName;
use yii\db\Migration;

/**
 * Class m000001_000001_command
 */
class m000001_000001_command extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        $this->createTable(TableName::COMMAND, [
            'id'          => $this->primaryKey(20),
            'id_user'     => $this->bigInteger(20)->notNull(),
            'pid'         => $this->bigInteger(20)->defaultValue(null),
            'progress'    => $this->bigInteger(20)->defaultValue(0),
            'ns'          => $this->string(1000)->notNull(),
            'is_running'  => $this->boolean()->notNull()->defaultValue(false),
            'created_at'  => $this->bigInteger(20)->notNull(),
            'updated_at'  => $this->bigInteger(20)->defaultValue(null),
            'finished_at' => $this->bigInteger(20)->defaultValue(null),
            'data'        => $this->text(),
            'response'    => $this->text(),
        ]);
    }

    /**
     *
     */
    public function safeDown()
    {
        $this->dropTable(TableName::COMMAND);
    }
}

<?php

namespace semako\queue\migrations;

use semako\queue\enums\TableName;
use yii\db\Migration;

/**
 * Class m000002_000002_command_added_priority
 */
class m000002_000002_command_added_priority extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        $this->addColumn(TableName::COMMAND, 'priority', $this->bigInteger(20)->notNull()->defaultValue(1));
    }

    /**
     *
     */
    public function safeDown()
    {
        $this->dropColumn(TableName::COMMAND, 'priority');
    }
}

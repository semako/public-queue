<?php

namespace semako\queue\enums;

/**
 * Class TableName
 * @package semako\queue\enums
 */
abstract class TableName
{
    const COMMAND = 'command';
}

<?php

namespace semako\queue\enums;

/**
 * Class CommandStatus
 * @package semako\queue\enums
 */
abstract class CommandStatus
{
    const OK   = 1;
    const FAIL = 0;
}

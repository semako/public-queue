<?php

namespace semako\queue\interfaces\events;

use semako\eventsManager\interfaces\IEvent;
use semako\queue\interfaces\models\ICommand;

/**
 * Interface IOnQueueCommandFailed
 * @package semako\queue\interfaces\events
 */
interface IOnQueueCommandFailed extends IEvent
{
    /**
     * @return ICommand
     */
    public function &getMessage();
}

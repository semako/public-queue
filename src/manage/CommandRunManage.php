<?php

namespace semako\queue\manage;

use yii\helpers\Json;
use semako\yii2Common\traits\ClassName;
use semako\queue\interfaces\ICommandRun;
use semako\queue\interfaces\models\ICommand;

/**
 * Class CommandRunManage
 * @package app\common\models\commands
 */
abstract class CommandRunManage implements ICommandRun
{
    use ClassName;

    /**
     * @var ICommand
     */
    private $command;

    /**
     * ACommand constructor.
     * @param ICommand $command
     */
    public function __construct(ICommand $command)
    {
        $this->command = $command;
    }

    /**
     * @return mixed
     */
    protected function getData()
    {
        return Json::decode($this->getCommand()->getData());
    }

    /**
     * @return ICommand
     */
    protected function getCommand()
    {
        return $this->command;
    }
}

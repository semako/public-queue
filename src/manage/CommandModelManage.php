<?php

namespace semako\queue\manage;

use semako\queue\enums\CommandStatus;
use semako\queue\interfaces\events\IOnQueueCommandFailed;
use semako\queue\interfaces\events\IOnQueueCommandFinished;
use semako\queue\interfaces\events\IOnQueueCommandStarted;
use semako\queue\interfaces\events\IOnQueueCommandUpdated;
use semako\queue\models\Command;

/**
 * Class CommandModelManage
 * @package semako\queue\manage
 */
class CommandModelManage
{
    /**
     * @param IOnQueueCommandStarted $event
     */
    public static function start(IOnQueueCommandStarted $event)
    {
        /** @var Command $command */
        $command = $event->getMessage();
        $command->is_running = true;
        $command->progress   = 0;
        $command->updated_at = time();
        $command->save();
    }

    /**
     * @param IOnQueueCommandFailed $event
     * @throws \Exception
     */
    public static function fail(IOnQueueCommandFailed $event)
    {
        /** @var Command $command */
        $command = $event->getMessage();
        $command->status = CommandStatus::FAIL;
        $command->save();
    }

    /**
     * @param IOnQueueCommandFinished $event
     */
    public static function finish(IOnQueueCommandFinished $event)
    {
        /** @var Command $command */
        $command = $event->getMessage();
        $command->is_running  = false;
        $command->progress    = 100;
        $command->finished_at = time();
        $command->updated_at  = time();
        $command->save();
    }

    /**
     * @param IOnQueueCommandUpdated $event
     */
    public static function update(IOnQueueCommandUpdated $event)
    {
        /** @var Command $command */
        $command = $event->getMessage();
        $command->is_running = true;
        $command->progress   = $event->getProgress();
        $command->updated_at = time();
        $command->save();
    }
}

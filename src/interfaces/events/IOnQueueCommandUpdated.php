<?php

namespace semako\queue\interfaces\events;

use semako\eventsManager\interfaces\IEvent;
use semako\queue\interfaces\models\ICommand;

/**
 * Interface IOnQueueCommandUpdated
 * @package semako\queue\interfaces\events
 */
interface IOnQueueCommandUpdated extends IEvent
{
    /**
     * @return ICommand
     */
    public function &getMessage();

    /**
     * @return int
     */
    public function getProgress();
}

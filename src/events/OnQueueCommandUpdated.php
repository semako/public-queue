<?php

namespace semako\queue\events;

use semako\eventsManager\components\Event;
use semako\queue\interfaces\events\IOnQueueCommandUpdated;

/**
 * Class OnQueueCommandUpdated
 * @package semako\queue\events
 */
class OnQueueCommandUpdated extends Event implements IOnQueueCommandUpdated
{
    /**
     * @var int
     */
    private $progress = 0;

    /**
     * Event constructor.
     * @param mixed $message
     * @param int $progress
     */
    public function __construct(&$message, $progress = 0) {
        $this->progress = (int) $progress;
        $this->progress = $this->progress < 0 ? 0 : ($this->progress > 100 ? 100 : $this->progress);
        parent::__construct($message);
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }
}

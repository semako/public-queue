<?php

namespace semako\queue\manage;

use semako\queue\models\Command;
use semako\yii2Common\interfaces\IResultBool;
use semako\yii2Common\interfaces\IManage;
use semako\queue\interfaces\ICommandRun;
use semako\queue\interfaces\models\ICommand;

/**
 * Class CommandProcessManage
 * @package semako\yii2Common\manage
 */
class CommandProcessManage implements IManage
{
    /**
     * @var ICommand
     */
    private $command;

    /**
     * ICommand constructor.
     * @param ICommand $command
     */
    public function __construct(ICommand $command)
    {
        $this->command = $command;
        $this->setCommandPid();
    }

    /**
     *
     */
    private function setCommandPid()
    {
        /** @var Command $command */
        $command = $this->command;
        $command->pid = getmypid();
        $command->save();
    }

    /**
     * @return IResultBool
     */
    public function run()
    {
        /** @var ICommandRun $object */
        $ns     = $this->command->getNs();
        $object = new $ns($this->command);

        return $object->run();
    }
}
